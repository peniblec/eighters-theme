(defun my/color-mix (color-1 color-2 ratio)
  (let* ((c1 (color-name-to-rgb color-1))
         (c2 (color-name-to-rgb color-2))
         (mix (-zip-with
               (lambda (i1 i2) (+ (* ratio i1)
                                  (* (- 1 ratio) i2)))
               c1 c2)))
    (apply 'color-rgb-to-hex `(,@mix 2))))

;; Alternate implementation without -zip-with from dash.
(defun my/color-mix (color-1 color-2 ratio)
  (let* ((c1 (color-name-to-rgb color-1))
         (c2 (color-name-to-rgb color-2))
         (mix (seq-map
               (lambda (pair) (+ (* ratio (car pair))
                                 (* (- 1 ratio) (cdr pair))))
               (cl-pairlis c1 c2))))
    (apply 'color-rgb-to-hex `(,@mix 2))))

;; Diff faces.

(setq my/background "black")

(list-colors-display
 (seq-map (lambda (r) (my/color-mix my/background "steelblue2" r))
          (number-sequence 0.0 1.0 0.01))
 "*steelblues*")

(list-colors-display
 (seq-map (lambda (r) (my/color-mix my/background "orange2" r))
          (number-sequence 0.0 1.0 0.01))
 "*oranges*")

(list-colors-display
 (seq-map (lambda (c) (my/color-mix my/background c 0.9))
          '("orange2" "steelblue2" "gold2" "maroon2"))
 "*regular-diff*")

(list-colors-display
 (seq-map (lambda (c) (my/color-mix my/background c 0.7))
          '("orange2" "steelblue2" "gold2" "maroon2"))
 "*refined-diff*")
